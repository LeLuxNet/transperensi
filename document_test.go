package transperensi_test

import (
	"fmt"
	"os"
	"testing"

	"lelux.net/transperensi"
)

const docEnv = "TRANSPERENSI_DOC"

func TestDocument(t *testing.T) {
	a := setup(t, false)

	doc := os.Getenv(docEnv)
	if doc == "" {
		t.Skip(docEnv + " env missing")
	}

	url, err := a.Document(transperensi.DocumentId(doc))
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(url)
}
