package transperensi

import (
	"fmt"
	"net/url"
	"strings"

	"lelux.net/x/html"
)

type RequestRole uint8

const (
	Obligor RequestRole = iota
	Anyone
)

type RequestBusiness uint16

const (
	GwG_2_1_10_a_aa RequestBusiness = 1 << iota
	GwG_2_1_10_a_bb
	GwG_2_1_10_a_cc
	GwG_2_1_10_a_dd
	GwG_2_1_10_a_ee
	GwG_2_1_10_b
	GwG_2_1_10_c
	GwG_2_1_10_d
	GwG_2_1_10_e
)

type RequestData struct {
	Role     RequestRole
	Business RequestBusiness
}

type RequestId string

var valuePSel = html.MustCompile(".value > p")

func (a *API) Request(requestUrl string, rd RequestData) (RequestId, error) {
	if a.StartUrl == "" {
		return "", ErrNotLoggedIn
	}

	data := url.Values{
		"step-list:0:step:dateRangeChoice": []string{"#radio-button-current"},
		"buttons:next":                     []string{""}}
	res, err := a.reqUrl(requestUrl, 0, data)
	if err != nil {
		return "", err
	}

	role := "obligor"
	if rd.Role == Anyone {
		role = "anyone"
	}

	data = url.Values{
		"step-list:0:step:role-choice": []string{"#radio-button-" + role},
		"buttons:next":                 []string{""}}
	res, err = a.req(res, 0, data)
	if err != nil {
		return "", err
	}

	data = url.Values{
		"step-list:2:step:gwg_10_3": []string{"on"},
		"buttons:next":              []string{""}}
	if rd.Business&GwG_2_1_10_a_aa != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_a_aa", "on")
	}
	if rd.Business&GwG_2_1_10_a_bb != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_a_bb", "on")
	}
	if rd.Business&GwG_2_1_10_a_cc != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_a_cc", "on")
	}
	if rd.Business&GwG_2_1_10_a_dd != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_a_dd", "on")
	}
	if rd.Business&GwG_2_1_10_a_ee != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_a_ee", "on")
	}
	if rd.Business&GwG_2_1_10_b != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_b", "on")
	}
	if rd.Business&GwG_2_1_10_c != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_c", "on")
	}
	if rd.Business&GwG_2_1_10_d != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_d", "on")
	}
	if rd.Business&GwG_2_1_10_e != 0 {
		data.Set("step-list:1:step:checkbox-group:gwg_2_1_10_e", "on")
	}

	res, err = a.req(res, 0, data)
	if err != nil {
		return "", err
	}

	data = url.Values{"buttons:finish": []string{""}}
	res, err = a.req(res, 0, data)
	if err != nil {
		return "", err
	}

	n, err := a.loadPage(res)
	if err != nil {
		return "", err
	}

	val := n.QueryAll(valuePSel)
	id := strings.TrimSpace(val[1].FirstChild.Data)

	fmt.Println(id)

	return RequestId(id), nil
}
