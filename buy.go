package transperensi

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/x/html"
)

type PaymentType string

const (
	CreditCard        PaymentType = "creditcard"
	SEPA              PaymentType = "elv"
	Invoice           PaymentType = "invoice"
	CollectiveInvoice PaymentType = "collective_invoice"
)

type BuyData struct {
	PaymentType PaymentType
}

var level1Sel = html.MustCompile(".level-1")

func (a *API) Buy(id RequestId, bd BuyData) (string, error) {
	if a.StartUrl == "" {
		return "", ErrNotLoggedIn
	}

	u := strings.Replace(a.StartUrl, "/start", "/antragsverwaltung", 1)
	data := url.Values{
		"document-request-list-panel:filter-form:param.request_number": []string{string(id)}}

	res, err := a.reqUrl(u, 0, data)
	if err != nil {
		return "", err
	}
	n, checkout, err := a.findCheckout(res, id)
	if err != nil {
		return "", err
	}
	if checkout == "" {
		ticker := time.NewTicker(15 * time.Minute)
		for range ticker.C {
			res, err = a.reqPage(res, n, 0, data)
			if err != nil {
				ticker.Stop()
				return "", err
			}
			n, checkout, err = a.findCheckout(res, id)
			if err != nil {
				ticker.Stop()
				return "", err
			}
			if checkout != "" {
				break
			}
		}
		ticker.Stop()
	}

	res, err = a.checkout(checkout, bd)
	if err != nil {
		return "", err
	}

	docUrl, err := a.document(res)

	return docUrl, err
}

func (a *API) findCheckout(res *http.Response, id RequestId) (*html.Node, string, error) {
	n, err := a.loadPage(res)
	if err != nil {
		return n, "", err
	}

	l1 := n.Query(level1Sel)
	if l1 == nil {
		return n, "", errors.New("transperensi: can't find request list")
	}
	idN := l1.FirstChild.NextSibling.NextSibling.NextSibling.FirstChild
	if idN.Data != string(id) {
		return n, "", fmt.Errorf("transperensi: can't find id %s in request list", id)
	}

	l2 := (*html.Node)(l1.NextSibling.NextSibling)
	if l2 == nil || !strings.HasPrefix(l2.GetAttr("class"), "level-2") {
		return n, "", nil
	}

	href := l2.Query(aSel).GetAttr("href")
	if href == "" {
		return n, "", errors.New("transperensi: document url should not be empty")
	}

	url, err := res.Request.URL.Parse(href)
	if err != nil {
		return n, "", err
	}

	return nil, url.String(), nil
}

func (a *API) checkout(cartUrl string, bd BuyData) (*http.Response, error) {
	a.cartLock.Lock()
	defer a.cartLock.Unlock()

	res, err := a.get(cartUrl)
	if err != nil {
		return nil, err
	}
	res.Body.Close()

	u := strings.Replace(a.StartUrl, "/start", "/warenkorb", 1)

	// cart
	data := url.Values{
		"cart-position-list:0:business_code": []string{""},
		"to-checkout-button":                 []string{""}}
	res, err = a.reqUrl(u, 0, data)
	if err != nil {
		return nil, err
	}

	// confirm cart
	data = url.Values{"buttons:next": []string{""}}
	res, err = a.req(res, 0, data)
	if err != nil {
		return nil, err
	}

	// payment type
	data = url.Values{
		"step-list:0:step:payment_type": []string{string(bd.PaymentType)},
		"buttons:next":                  []string{""}}
	res, err = a.req(res, 0, data)
	if err != nil {
		return nil, err
	}

	// buy
	dashType := strings.ReplaceAll(string(bd.PaymentType), "_", "-")
	data = url.Values{"buttons:finish-" + dashType: []string{""}}
	res, err = a.req(res, 0, data)
	if err != nil {
		return nil, err
	}

	data = url.Values{
		"buttons:back-to-start": []string{""}}
	return a.req(res, 0, data)
}
