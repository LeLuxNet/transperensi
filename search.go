package transperensi

import (
	"errors"
	"net/url"
	"strings"

	"golang.org/x/net/html/atom"
	"lelux.net/x/html"
)

var level0Sel = html.MustCompile(".level-0")
var aSel = html.MustCompile("a")

type SearchFilter struct {
	Office         string
	City           string
	ZIP            string
	Street         string
	RegisterNumber string
}

type SearchResult struct {
	Name       string
	Location   string
	RequestURL string
}

func (a *API) Search(text string, filter SearchFilter) ([]SearchResult, error) {
	if a.StartUrl == "" {
		return nil, ErrNotLoggedIn
	}

	data := url.Values{
		"param.search_text": []string{text},
		"search-button":     []string{""}}
	res, err := a.reqUrl(a.StartUrl, 0, data)
	if err != nil {
		return nil, err
	}

	data = make(url.Values)
	if filter.Office != "" {
		data.Set("param.office", filter.Office)
	}
	if filter.City != "" {
		data.Set("param.city", filter.City)
	}
	if filter.ZIP != "" {
		data.Set("param.zipcode", filter.ZIP)
	}
	if filter.Street != "" {
		data.Set("param.street", filter.Street)
	}
	if filter.RegisterNumber != "" {
		data.Set("param.register_number", filter.RegisterNumber)
	}
	if len(data) > 0 {
		res, err = a.req(res, 1, data)
		if err != nil {
			return nil, err
		}
	}

	p, err := a.loadPage(res)
	if err != nil {
		return nil, err
	}

	nList := p.QueryAll(level0Sel)

	list := make([]SearchResult, len(nList))
	for i, l0 := range nList {
		name := l0.LastChild.PrevSibling.FirstChild
		location := name.NextSibling.NextSibling

		l1 := (*html.Node)(l0.NextSibling.NextSibling)
		if l1.DataAtom != atom.Tr {
			return nil, errors.New("transperensi: expect second tr to follow")
		}

		links := l1.QueryAll(aSel)
		href := links[1].GetAttr("href")
		url, err := res.Request.URL.Parse(href)
		if err != nil {
			return nil, err
		}

		list[i] = SearchResult{
			Name:       html.TrimSpace(strings.TrimSpace(name.Data)),
			Location:   html.TrimSpace(strings.TrimSpace(location.Data)),
			RequestURL: url.String(),
		}
	}
	return list, nil
}
