package transperensi_test

import (
	"os"
	"testing"

	"lelux.net/transperensi"
)

const buyEnv = "TRANSPERENSI_BUY"

func TestBuy(t *testing.T) {
	a := setup(t, false)

	buy := os.Getenv(buyEnv)
	if buy == "" {
		t.Skip(buyEnv + " env missing")
	}

	_, err := a.Buy(transperensi.RequestId(buy), transperensi.BuyData{
		PaymentType: transperensi.CollectiveInvoice,
	})
	if err != nil {
		t.Fatal(err)
	}
}
