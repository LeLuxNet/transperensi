package transperensi

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"lelux.net/x/html"
)

var ErrNotLoggedIn = errors.New("transperensi: not logged in")

type API struct {
	Client *http.Client

	StartUrl string

	cartLock sync.Mutex
}

const baseUrl = "https://www.transparenzregister.de/treg/"

var formSel = html.MustCompile("form")
var hiddenInputSel = html.MustCompile("input[type=hidden]")

func (a *API) loadPage(page *http.Response) (*html.Node, error) {
	defer page.Body.Close()
	return html.Parse(page.Body)
}

func (a *API) get(url string) (*http.Response, error) {
	client := a.Client
	if client == nil {
		client = http.DefaultClient
	}
	return client.Get(url)
}

func (a *API) reqUrl(url string, formIndex int, data url.Values) (*http.Response, error) {
	res, err := a.get(url)
	if err != nil {
		return nil, err
	}
	return a.req(res, formIndex, data)
}

func (a *API) req(page *http.Response, formIndex int, data url.Values) (*http.Response, error) {
	n, err := a.loadPage(page)
	if err != nil {
		return nil, err
	}
	return a.reqPage(page, n, formIndex, data)
}

func (a *API) reqPage(page *http.Response, n *html.Node, formIndex int, data url.Values) (*http.Response, error) {
	forms := n.QueryAll(formSel)
	if len(forms) <= formIndex {
		return nil, fmt.Errorf("transperensi: formIndex=%d higher than forms on page=%d", formIndex, len(forms))
	}

	form := forms[formIndex]

	action := form.GetAttr("action")
	if action == "" {
		return nil, errors.New("transperensi: form has no action attribute")
	}
	url, err := page.Request.URL.Parse(action)
	if err != nil {
		return nil, fmt.Errorf("transperensi: could not parse action '%s': %w", action, err)
	}

	for _, val := range data {
		for i, v := range val {
			if strings.HasPrefix(val[0], "#") {
				sel, err := html.Compile(v)
				if err != nil {
					return nil, fmt.Errorf("transperensi: could not compile selector '%s': %w", v, err)
				}
				selN := form.Query(sel)
				val[i] = selN.GetAttr("value")
			}
		}
	}

	for _, input := range form.QueryAll(hiddenInputSel) {
		key := input.GetAttr("name")
		if !strings.HasPrefix(key, "csrf-") {
			continue
		}
		val := input.GetAttr("value")
		data.Set(key, val)
	}

	client := a.Client
	if client == nil {
		client = http.DefaultClient
	}
	return client.PostForm(url.String(), data)
}
