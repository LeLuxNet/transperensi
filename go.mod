module lelux.net/transperensi

go 1.18

require (
	golang.org/x/net v0.5.0
	lelux.net/x v0.0.0-20230131160406-230d14329e06
)

require github.com/andybalholm/cascadia v1.3.1 // indirect
