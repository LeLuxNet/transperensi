package transperensi_test

import (
	"os"
	"testing"

	"lelux.net/transperensi"
)

const emailEnv = "TRANSPERENSI_EMAIL"
const passwordEnv = "TRANSPERENSI_PASSWORD"

func setup(t *testing.T, login bool) *transperensi.API {
	email := os.Getenv(emailEnv)
	if email == "" {
		t.Fatal(emailEnv + " env missing")
	}

	password := os.Getenv(passwordEnv)
	if password == "" {
		t.Fatal(passwordEnv + " env missing")
	}

	var a transperensi.API
	err := a.Login(email, password)
	if err != nil {
		if login {
			t.Fatal(err)
		} else {
			t.Skip("failed to login")
		}
	}
	return &a
}

func TestLogin(t *testing.T) {
	a := setup(t, true)

	if a.StartUrl == "" {
		t.Fatal("expected API.StartUrl not to be empty after login")
	}
}
