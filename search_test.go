package transperensi_test

import (
	"testing"

	"lelux.net/transperensi"
)

func TestSearch(t *testing.T) {
	a := setup(t, false)

	res, err := a.Search("Google", transperensi.SearchFilter{})
	if err != nil {
		t.Fatal(err)
	}

	if len(res) != 1 {
		t.Fatalf("expected search to return 1 result, got %d", len(res))
	}

	name := "Google Germany GmbH"
	if res[0].Name != name {
		t.Fatalf("expected search result to be '%s', got '%s'", name, res[0].Name)
	}
}
