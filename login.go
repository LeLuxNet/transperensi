package transperensi

import (
	"errors"
	"net/url"
	"strings"
)

func (a *API) Login(email, password string) error {
	data := url.Values{
		"email":    []string{email},
		"password": []string{password}}
	res, err := a.reqUrl(baseUrl+"de/anmelden", 0, data)
	if err != nil {
		return err
	}
	res.Body.Close()

	url := res.Request.URL

	if strings.Contains(url.Path, "/anmelden") {
		return errors.New("transperensi: login credentials wrong")
	} else if !strings.Contains(url.Path, "/start") {
		return errors.New("transperensi: expected to be redirected to the start page after login")
	}

	url.RawQuery = ""
	a.StartUrl = url.String()

	return nil
}
