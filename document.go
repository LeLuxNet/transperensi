package transperensi

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	"lelux.net/x/html"
)

var companyNameSel = html.MustCompile(".company_name_name")

type DocumentId string

func (a *API) Document(id DocumentId) (string, error) {
	if a.StartUrl == "" {
		return "", ErrNotLoggedIn
	}

	u := strings.Replace(a.StartUrl, "/start", "/meinedaten/abrufe", 1)
	data := url.Values{
		"document-basket-list-panel:filter-form:basket_number": []string{string(id)}}

	res, err := a.reqUrl(u, 0, data)
	if err != nil {
		return "", err
	}
	n, err := a.loadPage(res)
	if err != nil {
		return "", err
	}

	l1 := n.Query(level1Sel)
	if l1 == nil {
		return "", errors.New("transperensi: can't find document list")
	}
	// TODO: Check Id match

	href := l1.Query(aSel).GetAttr("href")
	if href == "" || !strings.Contains(href, "DocumentBasketListPage") {
		return "", errors.New("transperensi: expected document list to link to document")
	}

	url, err := res.Request.URL.Parse(href)
	if err != nil {
		return "", err
	}

	res, err = a.get(url.String())
	if err != nil {
		return "", err
	}
	return a.document(res)
}

func (a *API) document(res *http.Response) (string, error) {
	n, err := a.loadPage(res)
	if err != nil {
		return "", err
	}

	l1 := n.Query(level1Sel)
	if l1 == nil {
		return "", errors.New("transperensi: can't find document list")
	}

	name := l1.Query(companyNameSel)
	if name == nil {
		return "", errors.New("transperensi: can't find document list")
	}

	link := l1.Query(aSel)
	if link == nil {
		u := res.Request.URL.String()

		ticker := time.NewTicker(30 * time.Second)
		defer ticker.Stop()
		for range ticker.C {
			res, err := a.get(u)
			if err != nil {
				return "", err
			}
			n, err := a.loadPage(res)
			if err != nil {
				return "", err
			}

			l1 := n.Query(level1Sel)
			if l1 == nil {
				return "", errors.New("transperensi: can't find document list")
			}
			link = l1.Query(aSel)
			if link != nil {
				break
			}
		}
		ticker.Stop()
	}

	u, err := res.Request.URL.Parse(link.GetAttr("href"))
	if err != nil {
		return "", err
	}

	return u.String(), nil
}
